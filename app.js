const express = require('express');
const app = express();
const PORT = 3000;

// GET Endpoint - http://localhost:3000 eli http://127.0.0.1:3000/
// req = require, res = response
app.get('/', (req, res) => {
    res.send('Hello World!');
});

/**
 * This function multiplies multiplicant with multiplier.
 * @param {number} multiplicant first param
 * @param {number} multiplier second param
 * @returns {number} product
 */
const multiply = (multiplicant, multiplier) => {
    const product = multiplicant * multiplier;
    return product;
};

// GET multiply endpoint - http://127.0.0.1:3000/multiply?a=3&b=5
app.get('/multiply', (req, res) => {
    try {
        const multiplicant = parseFloat(req.query.a);
        const multiplier = parseFloat(req.query.b);
        if (isNaN(multiplicant)) throw new Error('Invalid multiplicant value');
        if (isNaN(multiplier)) throw new Error('Invalid multiplier value');
        console.log({ multiplicant, multiplier });
        const product = multiply(multiplicant, multiplier);
        res.send(product.toString(10));
    } catch (err) {
        console.error(err.message);
        res.send("Couldn't calculate the product. Try again.");
    }
});

app.listen(PORT, () => console.log(
    `Listening at http://127.0.0.1:${PORT}`
));